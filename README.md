Docker Build Enviroment for harfbuzz-qml
========================================

Build
-----

The build downloads the debian base image and installs build dependencies.

```sh
docker build -f Dockerfile -t harfbuzz-qml
```

Run
---

Change into the source directory of harfbuzz-qml. Create the container:


```sh
docker create -v $PWD:/src --name harfbuzz-qml-build -t -i harfbuzz-qml
```

The source directory will be shared with the container in `/src`.

Then start the container:

```sh
docker container start harfbuzz-qml-build
```

Then start the build with:

```sh
docker container exec -w /build harfbuzz-qml-build cmake /src
docker container exec -w /build harfbuzz-qml-build make
```

Clean Up
--------

Remove the running container:

```sh
docker container stop harfbuzz-qml-build
docker container rm harfbuzz-qml-build
```

Delete the image:

```sh
docker image rm harfbuzz-qml
```
