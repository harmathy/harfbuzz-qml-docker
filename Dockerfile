FROM debian:stable

ENV LANG=C.UTF-8 DEBIAN_FRONTEND=noninteractive

RUN mkdir /build && \
    chmod 1777 /build && \
    apt-get update && \
    apt-get -y install --no-install-recommends \
       build-essential \
       cmake \
       libharfbuzz-dev \
       libfreetype6-dev \
       libfontconfig1-dev \
       qml-module-qtquick2 \
       qtbase5-dev \
       qtdeclarative5-dev \
       libkf5declarative-dev \

